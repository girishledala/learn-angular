//var ngApp = angular.module("myApplication",["ngRoute"]);
(function(window, document, angular, undefined) {

var ngApp = angular.module("myApplication",["ngRoute", "formly", "formlyBootstrap", "ngAnimate", "ngMessages"]);
	

	ngApp.run(function(formlyConfig, formlyValidationMessages,formlyApiCheck, $rootScope, $document, $window){
	
		$rootScope.documentClicked=function(event){
			$rootScope.$emit('documentClicked',event);
		};

		$document.on('click', $rootScope.documentClicked);

		formlyConfig.setType({
			name: 'customInput',
			extends: 'input',
			apiCheck: function(check){
				return{
					templateOptions:{
						foo: check.string.optional
					}
				};	
			},
			// templateUrl:"about.html"
		});

		formlyConfig.setType({
			name: 'customInput2',
			extends: 'input',
			apiCheck: function(check){
				return{
					templateOptions:{
						foo: check.string.optional
					}
				};	
			},
			// templateUrl:"projects.html"
		});
	});

	ngApp.config(function($routeProvider) {
	    $routeProvider
	    .when("/", {
	        templateUrl : "/About/about.html",
	        controller: 'aboutController'
	    })
	    .when("/learning", {
	        templateUrl : "/Learning/learning.html",
	        controller: 'learningController'
	    })
	    .when("/custom", {
	        templateUrl : "/Custom/custom.html",
	        controller: 'customFilter'
	    })
	    .when("/projects", {
	        templateUrl : "/Projects/projects.html",
	        controller: 'projectsController'
	    })
	    .when("/contact", {
	        templateUrl : "/Contact/contact.html",
	        controller: 'contactController'
	    })
	    .when("/location", {
	        templateUrl : "/Location/location.html",
	        controller: 'locationController'
	    });
	});

	ngApp.controller("custom", function($scope, $location, $timeout, $interval, $http, getMeNow){
		$scope.names = [{name: "Girish", dob: "11/11/2011", amount: 4},{name: "Sow", dob: "13/13/2013", amount: 5},{name: "Ram", dob: "12/12/2012", amount: 6} ];
		$scope.myURL = $location.absUrl();
		$scope.myMsg = "Hey Girish";
		$timeout(function() {
			$scope.myMsg = "How are you bud!!";
		}, 2000);
		$scope.date = new Date().toLocaleString();
		$interval(function(){
			$scope.date = new Date().toLocaleString();
		}, 1000);
		$http.get("https://httpbin.org/get").then(function(data){
			$scope.ajaxResponse = JSON.stringify(data);
		});

		// $http({method: 'POST', url: "states.json" }).then(function(result){
		// 	$scope.states = result.data;
		// });

		$scope.myNumber = getMeNow.getNumber(2);
		 
	});

	ngApp.controller("filtering", function($scope){
		$scope.name = [{name: "Girish"},{name: "Sow"},{name: "Ram"},{name: "Satish"},{name: "Geeta"},{name: "Surya"},{name: "Yash"} ]
	});

	ngApp.service("getMeNow", function(){
		
		var a = [10, 20, 30, 40, 50];
		this.getNumber = function(x){
			return a[x];
		}		
	});
})(window, document, angular);
	