(function() {
    var app = angular.module('myApplication');
	app.controller("learningController",function($scope){
		$scope.firstName = "GK";
		$scope.lastName = "LE";
		$scope.changeName = function(){
			$scope.firstName = "Girish";
			$scope.lastName = "Ledala";
		}
	});

	app.directive("gkTestDirective",function(){
		return {
			restrict : "EAC",
			template : "<h2>My directive</h2>"
		} 
	});
})();
