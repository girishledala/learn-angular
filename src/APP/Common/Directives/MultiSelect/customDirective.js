(function() {
    var app = angular.module('myApplication');
	app.directive("roMultiselect", function($rootScope, $document, $window){
		return {
			restrict: "E",
			replace : true,
			transclude:true,
			scope: {"label": "@", "options": "=", "onSubmit": "="},

			link : function(scope, ele, attrs){
				console.log(scope);
				scope.selectedVal = [];
				scope.showing = false;
				scope.summary = "";
				var container=ele.children('.selectedVal');
			},

			controller: function($scope, $rootScope, $element, $window){
				// $scope.onSubmit("ss");

				var container=$element.children('.selectedVal');

				$rootScope.$on('documentClicked',function(event,data){
	        		var clickedEle = data.target;
	        		console.log($element[0]);	

	        		if(!($element[0].contains(clickedEle))){
	        			$scope.showing = false;
	        		}
	        		else{

	        		}
	        	});

				$scope.showValue = function(e){
					$scope.showing = !($scope.showing);
					if($scope.showing){
						container.addClass("focus");
					}
					else{
						container.removeClass("focus");
					}
				}

				$scope.toggleOption = function(option){
					var found = $scope.findIndex(option.id);
					if(found == -1){
						$scope.selectedVal.push(option);
					}else{
						$scope.selectedVal.splice(found,1);
					}
				}

				$scope.selectAll = function(){
					$scope.selectedVal = [];
					angular.forEach($scope.options, function(option){
						$scope.selectedVal.push(option)
					});	
				}

				$scope.selectNone = function(){
					$scope.selectedVal = [];
				}

				$scope.$watchCollection("selectedVal",function(newV){
					if(newV){
						
						if(newV.length == 1){
							$scope.summary = newV[0].name;
						}
						if(newV.length > 1){
							$scope.summary = newV.length +' Selected';
						}
						if(newV.length == $scope.options.length){
							$scope.summary = "All Selected";
						}
						if(newV.length == 0){
							$scope.summary = "";
						}
					}
				})
				$scope.findIndex = function (option_id){
					var index = -1;
					for (var i = 0 ; i < $scope.selectedVal.length; i++){
						if(option_id == $scope.selectedVal[i].id){
							index = i;
						}
					}
					return index;
				}

			},

			template: "<div class='col-md-6'><div class='selectedVal col-md-12' ng-click='showValue(element)'>{{summary}}</div><div ng-show='showing' class='dropdownValues bottom col-md-12'><input type='submit' ng-click='selectAll()' value='Select All'/><div><input type='submit' ng-click='selectNone()' value='Select None'/><div><input ng-model='searchText' class='search col-md-10' style='padding-top:10px'/></div> <div ng-transclude></div><div ng-repeat='option in options | filter: searchText' ng-click='toggleOption(option)'> <span class='image'>{{findIndex(option.id)== -1 ? 'X':'O'}}</span> <span class='text'>{{option.name}}</span></div></div></div>"
		};
	});
})();

