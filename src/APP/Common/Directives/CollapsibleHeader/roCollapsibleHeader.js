/**
 * 
 */
(function() {
    'use strict';    
    var app = angular.module('myApplication');
	app.directive("roCollapseableHeader", ['$rootScope',function($rootScope) {
		return {
			restrict: "E",
			replace: true,
			transclude: true,
			scope: { isCollapsed:"=", isHighlight:"=", headerKey:"@"},
			link: function(scope) {
				scope.updateStatus = function(headerKey) {
					scope.isCollapsed=!scope.isCollapsed;
					if(headerKey){
						var flag=headerKey.split('.');
						flag=flag[flag.length-1];
						$rootScope.$broadcast(flag);
					}
	            }
	        },
			template:   '<div><div class="ro-collapseable-header"  style="margin-top: 20px">'+
			            '    <span class="disable-select ro-collapseable-header-title" translate="{{headerKey}}"></span>'+
			            '    <button type="button" ng-click="updateStatus(headerKey);" class="no-print disable-select ro-collapseable-header-resize" ng-class="{\'active\':isHighlight }">'+
				        '        <i ng-show="isCollapsed" class="glyphicon glyphicon-resize-small"></i> '+
				        '        <i	ng-show="!isCollapsed"	class="glyphicon glyphicon-resize-full"></i>'+
			            '    </button>'+
		                '</div><section ng-transclude ng-if="!isCollapsed"></section></div>'
		};
	}]);
})();