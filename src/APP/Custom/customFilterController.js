(function() {
    var app = angular.module('myApplication');
	app.controller("customFilter",function($scope, $http){
		var fc = this;
		
		fc.text = 'Custom Select';
		fc.tempObject=[];

		$http({method: 'GET', url: "../JSON/dropdowndata.json", data : '' }).then(function(result){
			fc.tempObject = result.data;
			console.log(result.data);

		});

		fc.openHeader = false;
		fc.highlight = true;

		fc.submit = function(resp){
			console.log(resp);
		}
		
	});

	app.controller("formlyController",function($scope,$http){
		var fc = this;

		fc.model = {};
		fc.options = {};
   	
		fc.onSubmit = onSubmit;

		fc.fields = [
			{
				key : 'firstName',
				type: 'customInput',
				templateOptions: {
					required: true,
					label: 'First Name',
					foo : 'hi'
				}
			},
			{
				key : 'lastName',
				type: 'customInput2',
				templateOptions: {
					label: 'Last Name'
				},
				expressionProperties:{
					'templateOptions.disabled' : '!model.firstName'
				}
			},
			{
				template: '<div><b>Address:<b></div>'
			},
			{
				key : 'address',
				type: 'input',
				templateOptions: {
					label: 'Street ',
					required: true
				}
			},
			{


				key : 'state',
				type: 'select',
				templateOptions: {
					label: 'State ',
					options:[],
					required: true,
					 valueProp: 'abbreviation',
          			labelProp: 'name',
				},
				controller: function($scope, $http) {
					$http.get('../JSON/states.json').then(function(response)
					{
						$scope.to.options = response.data;
						return response;

					});
  				}
   
			},
			{
				key: 'verifyField',
				type:'checkbox',
				templateOptions:{
					label: 'Check this if you can'
				}
			},
			{
		        key: 'ipAddress',
		        type: 'input',
		        templateOptions: {
			        label: 'IP Address',
			        placeholder: '127.0.0.1'
	       		},
	       		hideExpression: '!model.verifyField',
		        validators: {
		          	ipAddress: {
		            	expression: function(viewValue, modelValue) {
		              		var value = modelValue || viewValue;
		              		return !value || /(\d{1,3}\.){3}\d{1,3}/.test(value);
		            	},
		            message: '$viewValue + " is not a valid IP Address"'
		          }
		        }
	       	}
		];

		function onSubmit() {
		    //fc.options.updateInitialValue();
		    alert(JSON.stringify(fc.model), null, 2);
	    }
	});

})();
